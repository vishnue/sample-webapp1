'use strict';

angular.module('myApp').factory('RecipeService', ['$http', '$q', function($http, $q) {
  var REST_SERVICE_URI = 'http://localhost:8080/sample-webapp1/v1/recipe/list';

  var factory = {
    fetchAllRecipes: fetchAllRecipes
  };

  return factory;

  function fetchAllRecipes() {
    var deferred = $q.defer();

    $http.get(REST_SERVICE_URI)
      .then(
      function (response) {
        deferred.resolve(response.data);
      },
      function(errResponse){
        console.error('Error while fetching Users');
        deferred.reject(errResponse);
      }
    );

    return deferred.promise;
  }

}]);
