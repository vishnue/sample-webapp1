$(document).ready(function(){
  $("#createCommentContainer").delegate("#commentSubmitButton", "click", function(evt){
    evt.preventDefault();
    createComment();
  });

  function createComment(){
    var name = $("#name").val();
    var description = $("#description").val();

    var data = {};
    data.name = name;
    data.description = description;

    var url = "http://localhost:8080/sample-webapp1/v1/comment/create";

    $.post(url, data, function(response) {
          window.location.href = "http://localhost:8080/sample-webapp1/comment/list";
        });
  }
});



/*$(document).ready(function() {
  $("#createRecipeContainer").delegate("#recipeSubmitButton", "click", function(evt) {
    evt.preventDefault();
    createRecipe();
  });

  function createRecipe() {
    var author = $("#author").val();
    var name = $("#name").val();
    var ingredients = $("#ingredients").val();

    var data = {};
    data.author = author;
    data.name = name;
    data.ingredients = ingredients;

    var url = "http://localhost:8080/sample-webapp1/v1/recipe/create";

    $.post(url, data, function(response) {
      window.location.href = "http://localhost:8080/sample-webapp1/recipe/list";
    });
  }

});
*/
