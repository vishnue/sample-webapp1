$(document).ready(function() {
  $("#createRecipeContainer").delegate("#recipeSubmitButton", "click", function(evt) {
    evt.preventDefault();
    createRecipe();
  });

  function createRecipe() {
    var author = $("#author").val();
    var name = $("#name").val();
    var ingredients = $("#ingredients").val();

    var data = {};
    data.author = author;
    data.name = name;
    data.ingredients = ingredients;

    var url = "http://localhost:8080/sample-webapp1/v1/recipe/create";

    $.post(url, data, function(response) {
      window.location.href = "http://localhost:8080/sample-webapp1/recipe/list";
    });
  }

});

/*
$document.ready(function() {
  $("#feedContentWrap").delegate("#postButton", "click", function(evt) {
	  evt.preventDefault();
	  createPost(this);
  });

}
  function createRecipe(el) {
    var valid = true;
    var author = $("#author").val();
    var name = $( "#name" ).val();
    var ingredients = $( "ingredients" ).val();
    }
    var data = {};
    data.author= author;
        data.name = name;
        data.ingredients = ingredients;
      var url = "/v1/recipe/create";
        var html = $("#loadingTmpl").render({message : 'Posting...'});
        $("#loading .modal-body").html(html);
        $("#loading").modal('show');
         $.post(url, data, function(response){
        	if (response.success) {
        	  var html = $("#recipesTemplate"").render(response.data);
        	  var $container = $('#postsModelContainer');
        	  $container.prepend(html);
        	  $("#author").val('');
        	  $("#name").val('');
        	  $("#ingredients").val('');
        	} else {
        	}
          }, 'json');

};
*/
