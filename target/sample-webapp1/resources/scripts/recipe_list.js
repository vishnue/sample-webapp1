$(document).ready(function() {
  $("#listContainer").delegate(".recipeDeleteLink", "click", function(evt){
	   evt.preventDefault();
	   var id = $(this).data("id");
     var url = "http://localhost:8080/sample-webapp1/v1/recipe/delete?id=" + id;
     $.getJSON(url, function(response) {
       if (response) {
         $("#recipe_" + id).remove();
       }
     });
  });

  var url = "http://localhost:8080/sample-webapp1/v1/recipe/list";
  $.getJSON(url, function(response){
    if (response) {
      var json = {};
      json.recipeList = response;
      var htmlCode = $("#recipesTemplate").render(json);
      $("#listContainer").html(htmlCode);
    }
  });

});
