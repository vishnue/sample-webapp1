<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page isELIgnored="false" %>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <script src="/sample-webapp1/resources/scripts/recipe_create.js"></script>
  </head>

  <body>
    <%@ include file="/WEB-INF/jsp/header.jsp"%>


      <div id="createRecipeContainer">
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
          <h2>Recipe Form</h2>
          <form role="form">
          <div class="form-group">
            <label for="author">Author</label>
            <input type="text" class="form-control" id="author" name="author">
          </div>
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name">
          </div>

          <div class="form-group">
            <label for="ingredients">Ingredients</label>
            <input type="" class="form-control" id="ingredients" name="ingredients">
          </div>

          <button type="submit" id="recipeSubmitButton" class="btn btn-default">Submit</button>
        </form>
      </div>
    </div>
  </body>
</html>

