<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Recipe Details</title>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
     <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.77/jsrender.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.77/jsrender.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.77/jsrender.min.js.map"></script>

     <script src="/sample-webapp1/resources/scripts/comment_list.js"></script>
     <script src="/sample-webapp1/resources/scripts/recipe_details.js"></script>
  </head>

  <body>
    <%@ include file="/WEB-INF/jsp/header.jsp"%>
    <script>
      var recipeId = "${recipeDetails.id}";
    </script>

    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <h2><c:out value="${recipeDetails.name}"/></h2>
        <p><c:out value="${recipeDetails.ingredients}"/></p>
      </div> <!-- end of column -->
    </div> <!-- end of row -->

    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div id="listContainer">
        </div>
      </div><!--end of column-->
    </div> <!-- row -->

    <script id="commentsTemplate" type="text/x-jsrender">
      <h3>Comments</h3>
      <table  class="table table-striped">
        <tbody id="commentsContainer">

           {{for commentList tmpl="#commentTemplate" /}}

        </tbody>
      </table>
    </script>

    <script id="commentTemplate" type="text/x-jsrender">
      <tr><td>{{>description}}</td></tr>
    </script>

    <div id="createCommentContainer">
      <div class="row"><div class="col-md-4 col-md-offset-4">
        <h3>Comment Form</h3>
        <form class="form-inline">
            <div class="form-group">
              <label for="description">Description</label>
              <input type="text" class="form-control" id="description" name="description">
            </div>

            <button type="submit" class="btn btn-default" id="commentSubmitButton">Submit</button>
        </form>
      </div> <!-- end of row & column -->
    </div> <!-- end of createCommentContainer -->
  </body>
</html>
