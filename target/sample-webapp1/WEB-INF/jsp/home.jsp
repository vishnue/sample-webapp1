<!DOCTYPE html>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
  <%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
  <%@ page isELIgnored="false" %>
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title></title>
   </head>
  <body>
     <h1>Welcome SpingMVC4 Home Page! ${name} </h1>
     <h2>Person name: ${person.name} </h1>
     <h2>Person age: ${person.age} </h1>
  </body>
</html>
