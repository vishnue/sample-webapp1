<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page isELIgnored="false" %>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.77/jsrender.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.77/jsrender.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.77/jsrender.min.js.map"></script>
    <script src="/sample-webapp1/resources/scripts/recipe_details_modified.js"></script>
  </head>

  <script>
    var recipeId = "${id}";
  </script

  <body>
    <%@ include file="/WEB-INF/jsp/home.jsp"%>

    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div id="detailsContainer">
        </div>
      </div>
    </div>

    <script id="recipeTmpl" type="text/x-jsrender">
      <table class="table table-bordered" style="margin-bottom: 0px">
        <tbody>
            <tr><td>Name</td><td>{{>name}}</td></tr>
            <tr><td>Ingredients</td><td>{{>ingredients}}</td></tr>
        </tbody>
      </table>
    </script>
  </body>
</html>

