<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page isELIgnored="false" %>

<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
      <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </head>
</head>
<body>

     <%@ include file="/WEB-INF/jsp/header.jsp"%>

 <div class="container-fluid text-center">
   <div class="row">
        <p>Thanks for submitting the recipe!</p>
        <a href="/sample-webapp1/recipe/form">Create another recipe.</a>
        <p>Click below link to get all the list of recipes</p>
        <a href="/sample-webapp1//recipe/list">Get list of recipes</a>
    </div>
 </div>
</body>
</html>
