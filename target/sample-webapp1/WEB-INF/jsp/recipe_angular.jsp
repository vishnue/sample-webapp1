<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <title>AngularJS Recipes Example</title>
    <style>
    </style>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  </head>

  <body ng-app="myApp" class="ng-cloak">
      <div class="generic-container" ng-controller="RecipeController as ctrl">
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">List of Recipes </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>Author</th>
                              <th>Name</th>
                              <th>Ingredients</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="recipe in ctrl.recipes">
                              <td><span ng-bind="recipe.author"></span></td>
                              <td><span ng-bind="recipe.name"></span></td>
                              <td><span ng-bind="recipe.ingredients"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="/sample-webapp1/resources/angular/app.js"></script>
    <script src="/sample-webapp1/resources/angular/service/recipe_service.js"></script>
    <script src="/sample-webapp1/resources/angular/controller/recipe_controller.js"></script>
  </body>
</html>
