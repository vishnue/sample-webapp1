<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li ><a href="/sample-webapp1/recipe/home">RECIPE BOX</a></li>
        <li class="active"><style>.active { background-color: #4CAF50;}</style><a href="#">About</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        <li><a href="/sample-webapp1/recipe/form">Create a Recipe</a></li>
        <li><a href="/sample-webapp1/recipe/list">List of Recipe</a></li>
      </ul>
    </div>
  </div>
</nav>
