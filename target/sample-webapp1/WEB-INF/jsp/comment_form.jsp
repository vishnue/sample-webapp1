<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page isELIgnored="false" %>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

     <script src="/sample-webapp1/resources/scripts/comment_create.js"></script>
  </head>

  <body>
    <%@ include file="/WEB-INF/jsp/header.jsp"%>
    <div id="createCommentContainer">
      <div class="row"><div class="col-md-6 col-md-offset-3">
        <h2>Comment Form</h2>
        <form role="form">
          <div class="form-group">
            <label for="name">name</label>
            <input type="text" class="form-control" id="name" name="name">
          </div>
          <div class="form-group">
            <label for="description">Description</label>
            <input type="text" class="form-control" id="description" name="description">
          </div>

          <button type="submit" class="btn btn-default" id="commentSubmitButton">Submit</button>
        </form>
      </div></div> <!-- end of row & column -->
    </div> <!-- end of createCommentContainer -->
  </body>
</html>

