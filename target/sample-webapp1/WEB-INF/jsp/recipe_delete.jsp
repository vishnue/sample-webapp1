<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.77/jsrender.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.77/jsrender.min.js"></script
<script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.77/jsrender.min.js.map"></script>

<script src="/sample-webapp1/resources/scripts/recipe_list.js"></script>

</head>

<body>
  <%@ include file="/WEB-INF/jsp/com_layout.jsp"%>

 <div class="row">
   <div class="col-md-6 col-md-offset-3">
       <div id="listContainer">
       </div>
   </div>
 </div> <!-- row -->

  <script id="recipesTemplate" type="text/x-jsrender">
     <table class="table table-bordered" style="margin-bottom: 0px">
       <thead>
         <tr>
           <th>recipeName</th>
           <th>recipeIngredients</th>
           <th>recipeAuthor</th>
           <th>editRecipe</th>
           <th>recipeDetails</th>
           <th>recipeDelete</th>
          </tr>
       </thead>
       <tbody>
          {{for recipeList tmpl="#recipeTemplate" /}}
       </tbody>
    </table>
   </script>

   <script id="recipeTemplate" type="text/x-jsrender">
    <tr id="recipe_{{>id}}">
      <td>{{>name}}</td>
      <td>{{>ingredients}}</td>
      <td>{{>author}}</td>
      <td><a href="/sample-webapp1/recipe/update?id={{>id}}">EditRecipe</a></td>
      <td><a href="/sample-webapp1/recipe/details?id={{>id}}">recipeDetails</a></td>
      <!--td><a href="/sample-webapp1/recipe/delete?id={{>id}}">recipeDelete</a></td-->
      <td><a class="recipeDeleteLink" data-id="{{>id}}">recipeDelete</a></td>
    </tr>
   </script>
</body>
</html>
