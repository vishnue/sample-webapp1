<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }

    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}

    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }

    /* Set black background color, white text and some padding */

    myNavbar.a{
       color: orange;
    }

    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;}
    }
    li a.active {
        color: white;
        background-color: #4CAF50;
    }

  </style>
</head>

<body>

<%@ include file="/WEB-INF/jsp/header.jsp"%>

<div class="container-fluid text-center">
  <div class="row">
    <div class="col-sm-8 text-center col-sm-offset-2">
      <style> h2.italic {font-style: italic;}</style>
      <style>h2 {color: green}</style>
      <h2 class="italic">Welcome to Recipe Box</h2>
      <style>
       #rcorners6 {
          border-radius: 15px 50px;
          padding: 20px;
          width: 200px;
          height: 150px;
           }
      </style>
      <img id ="rcorners6" src="http://netnebraska.org/sites/default/files/styles/basic-page-feature-image-standalone/public/page/recipebox_featureimage.jpg?itok=7tWbC8_I" alt="Mountain View" style="width:350px;height:300px;">
     </div>
   </div>
</div>
    <div class="container text-center" style=" margin-top:20px ; padding-top:10px">
      <div class="row">
        <div class="col-sm-4 col-sm-offset-2">
           <div class="well">
              <p>Appetizers</p>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="well">
               <p>Main Course</p>
            </div>
         </div>
      </div> <!-- container -->
    </div>
</body>
</html>
