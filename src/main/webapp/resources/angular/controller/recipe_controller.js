'use strict';

angular.module('myApp').controller('RecipeController', ['$scope', 'RecipeService', function($scope, RecipeService) {
  var self = this;
  self.recipes = [];

  fetchAllRecipes();

  function fetchAllRecipes(){
    RecipeService.fetchAllRecipes()
      .then(
      function(recipes) {
        self.recipes = recipes;
      },
      function(errResponse){
        console.error('Error while fetching recipes');
      }
    );
  }

}]);
