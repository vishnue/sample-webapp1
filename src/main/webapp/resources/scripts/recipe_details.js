$(document).ready(function(){
  $("#createCommentContainer").delegate("#commentSubmitButton", "click", function(evt){
    evt.preventDefault();
    createComment();
  });

  function createComment(){
    var description = $("#description").val();

    var data = {};
    data.description = description;
    data.rid = recipeId;
    var url = "http://localhost:8080/sample-webapp1/v1/comment/create";

    $.post(url, data, function(response) {
      $("#description").val('');
      //window.location.href = "http://localhost:8080/sample-webapp1/comment/list";
      if (response){
        var htmlCode = $("#commentTemplate").render(response);
        $("#commentsContainer").append(htmlCode);
      }

    });
  }
});
