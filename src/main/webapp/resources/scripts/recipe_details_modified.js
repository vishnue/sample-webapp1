$(document).ready(function() {
  var url = "http://localhost:8080/sample-webapp1/v1/recipe/details?id=" + recipeId;
  $.getJSON(url, function(response){
    if (response) {
      var htmlCode = $("#recipeTmpl").render(response);
      $("#detailsContainer").html(htmlCode);
    }
  });
});
