<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="/sample-webapp1/resources/scripts/recipe_details_modified.js"></script>

  <script>
    var recipeId = "${id}";
  </script


    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div id="detailsContainer">
        </div>
      </div>
    </div>

    <script id="recipeTmpl" type="text/x-jsrender">
      <table class="table table-bordered" style="margin-bottom: 0px">
        <tbody>
            <tr><td>Name</td><td>{{>name}}</td></tr>
            <tr><td>Ingredients</td><td>{{>ingredients}}</td></tr>
        </tbody>
      </table>
    </script>


