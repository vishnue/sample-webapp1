<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="/sample-webapp1/resources/scripts/recipe_create.js"></script>

   <div id="createRecipeContainer">
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
          <h2>Recipe Form</h2>
          <form role="form">
          <div class="form-group">
            <label for="author">Author</label>
            <input type="text" class="form-control" id="author" name="author">
          </div>
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name">
          </div>

          <div class="form-group">
            <label for="ingredients">Ingredients</label>
            <input type="" class="form-control" id="ingredients" name="ingredients">
          </div>

          <button type="submit" id="recipeSubmitButton" class="btn btn-default">Submit</button>
        </form>
      </div>
    </div>


