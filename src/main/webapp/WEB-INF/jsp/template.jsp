<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page isELIgnored="false" %>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.77/jsrender.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.77/jsrender.min.js"></script
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.77/jsrender.min.js.map"></script>
  </head>

	<body>
	  <%@ include file="/WEB-INF/jsp/header.jsp"%>

	  <div id="contentWrap">
      <jsp:include page="/WEB-INF/jsp/${_content}.jsp" />
	  </div>
	</body>

</html>
