  <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <h2>Recipe Form</h2>
        <form role="form" action="/sample-webapp1/recipe/update?id=${recipeUpdate.id}" method="post">
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="${recipeUpdate.name}">
          </div>
          <div class="form-group">
            <label for="ingredients">Ingredients</label>
            <input type="text" class="form-control" id="ingredients" name="ingredients" value="${recipeUpdate.ingredients}">
          </div>


          <button type="submit" class="btn btn-default">Submit</button>
        </form>
      </div>
    </div>
