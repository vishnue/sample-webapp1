<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="/sample-webapp1/resources/scripts/recipe_list.js"></script>

<div class="row"><div class="col-md-6 col-md-offset-3">
  <div id="listContainer">
  </div>
</div></div> <!-- row -->

<script id="recipesTemplate" type="text/x-jsrender">
 <table class="table table-bordered" style="margin-bottom: 0px">
   <thead>
     <tr>
       <th>recipeName</th>
       <th>editRecipe</th>
       <th>recipeDetails</th>
       <th>recipeDelete</th>
      </tr>
   </thead>
   <tbody>
    {{for recipeList tmpl="#recipeTemplate" /}}
   </tbody>
 </table>
</script>

<script id="recipeTemplate" type="text/x-jsrender">
  <tr id="recipe_{{>id}}">
    <td>{{>name}}</td>
    <td><a href="/sample-webapp1/recipe/update?id={{>id}}">EditRecipe</a></td>
    <td><a href="/sample-webapp1/recipe/details?id={{>id}}">recipeDetails</a></td>
    <!--td><a href="/sample-webapp1/recipe/delete?id={{>id}}">recipeDelete</a></td-->
    <td><a class="recipeDeleteLink" data-id="{{>id}}">recipeDelete</a></td>
  </tr>
</script>
