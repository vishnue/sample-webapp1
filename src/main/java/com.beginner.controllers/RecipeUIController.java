package com.beginner.controllers;

import com.beginner.dao.MongoCommentDao;
import com.beginner.dao.MongoRecipesDao;
import com.beginner.domain.Recipe;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller public class RecipeUIController {

  @Value("#{recipesDao}") private MongoRecipesDao recipesDao;
  @Value("#{commentDao}") private MongoCommentDao commentDao;

  @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String displayEnterpage() {
      return "recipe_enter";
    }

  @RequestMapping(value = "/recipe/home", method = RequestMethod.GET)
  public String displayHomepage() {
    return "recipe_home";
  }

  @RequestMapping(value = "/recipe/form", method = RequestMethod.GET)
  public String displayRecipeForm() {
    return "recipe_form_view";
  }

  @RequestMapping(value = "/comment/form", method = RequestMethod.GET)
  public String displayCommentForm() {
    return "comment_form";
  }

  @RequestMapping(value = "/recipe/update", method = RequestMethod.GET)
  public String updateRecipe(Model model, HttpServletRequest request) {
    String id = request.getParameter("id");
    // model.addAttribute("recipeUpdate", recipesDao.getRecipe(id));
    return "recipe_update_view";
  }

  @RequestMapping(value = "/recipe/update", method = RequestMethod.POST)
  public String updateRecipe(HttpServletRequest request) {
    String recipeName = request.getParameter("name");
    String recipeId = request.getParameter("id");
    String recipeAuthor = request.getParameter("author");
    String recipeIngredients = request.getParameter("ingredients");
    //String id = String.format("recipe_%s", new ObjectId().toString());
    Recipe recipe =
        new Recipe(recipeId, recipeName, recipeIngredients, recipeAuthor);
    recipesDao.updateRecipe(recipe);
    return "recipe_success_view";
  }

  @RequestMapping(value = "/recipe/list", method = RequestMethod.GET)
  public String displayRecipe(Model model, HttpServletRequest request) {
    //model.addAttribute("recipeList", recipesDao.getRecipes());
    return "recipe_list_view";

  }

  @RequestMapping(value = "/comment/list", method = RequestMethod.GET)
  public String displayComment(Model model, HttpServletRequest request) {
    //model.addAttribute("recipeList", recipesDao.getRecipes());
    return "comment_list";
  }

  @RequestMapping(value = "/recipe/details", method = RequestMethod.GET)
  public String detailRecipe(Model model, HttpServletRequest request) {
    String id = request.getParameter("id");
    model.addAttribute("recipeDetails", recipesDao.getRecipe(id));
    return "recipe_details_view";
  }

  @RequestMapping(value = "recipe/details/modified", method = RequestMethod.GET)
  public String detailRecipeModified(Model model, HttpServletRequest request) {
    String id = request.getParameter("id");
    model.addAttribute("id", id);
    return "recipe_details_modified_view";
  }

  @RequestMapping(value = "recipe/delete", method = RequestMethod.GET)
  public String detailRecipeDelete(Model model, HttpServletRequest request) {
    String id = request.getParameter("id");
    recipesDao.removeRecipe(id);
    return "success_view";
  }

  @RequestMapping(value = "recipe/angular_example", method = RequestMethod.GET)
  public String angularExample() {
    return "recipe_angular";
  }
}
