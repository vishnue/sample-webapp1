package com.beginner.controllers;

import com.beginner.domain.Person;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller public class HomeController {

  @RequestMapping(value = "/home", method = RequestMethod.GET)
  public String viewHome(Model model) {
    model.addAttribute("name", "Praveen");

    Person vishnu1 = new Person("Vishnu", 26);
    model.addAttribute("person", vishnu1);

    return "home";
  }
}
