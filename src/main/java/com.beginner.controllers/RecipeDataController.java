package com.beginner.controllers;

import com.beginner.dao.MongoCommentDao;
import com.beginner.dao.MongoRecipesDao;
import com.beginner.domain.Comment;
import com.beginner.domain.Recipe;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

@Controller public class RecipeDataController {

  @Value("#{recipesDao}") private MongoRecipesDao recipesDao;
  @Value("#{commentDao}") private MongoCommentDao commentDao;

  @RequestMapping(value = "v1/recipe/create", method = RequestMethod.POST)
  public @ResponseBody Recipe createRecipe(HttpServletRequest request) {
    String recipeName = request.getParameter("name");
    String recipeIngredients = request.getParameter("ingredients");
    String recipeAuthor = request.getParameter("author");
    String id = String.format("recipe_%s", new ObjectId().toString());
    Recipe recipe = new Recipe(id, recipeName, recipeIngredients, recipeAuthor);
    recipesDao.insertRecipe(recipe);
    return recipesDao.getRecipe(id);
  }

  @RequestMapping(value = "v1/comment/create", method = RequestMethod.POST)
  public @ResponseBody Comment createComment(HttpServletRequest request) {
    String id = String.format("comment_%s", new ObjectId().toString());
    String commentDescription = request.getParameter("description");
    String commentName = request.getParameter("name");
    String recipeID = request.getParameter("rid");
    Comment comments =
        new Comment(id, commentDescription, commentName, recipeID);
    commentDao.insertComment(comments);
    return commentDao.getComment(id);
  }


  @RequestMapping(value = "v1/recipe/list", method = RequestMethod.GET) public
  @ResponseBody List<Recipe> listRecipe(HttpServletRequest request) {
    //String id = request.getParameter("id");
    return recipesDao.getRecipes();
  }


  @RequestMapping(value = "v1/comment/list", method = RequestMethod.GET) public
  @ResponseBody List<Comment> listComment(HttpServletRequest request) {
    String id = request.getParameter("id");
    return commentDao.getComment();
  }

  @RequestMapping(value = "v1/recipe/details", method = RequestMethod.GET)
  public @ResponseBody Recipe detailRecipe(HttpServletRequest request) {
    String id = request.getParameter("id");
    return recipesDao.getRecipe(id);
  }

  @RequestMapping(value = "v1/recipe/delete", method = RequestMethod.GET)
  public @ResponseBody JsonResponse deleteRecipe(HttpServletRequest request) {
    String id = request.getParameter("id");
    recipesDao.removeRecipe(id);
    JsonResponse response = new JsonResponse();
    response.success = true;
    return response;
  }

  private static class JsonResponse {
    public Object data;
    public boolean success;
  }


}
