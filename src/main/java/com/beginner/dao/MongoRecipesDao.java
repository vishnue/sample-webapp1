package com.beginner.dao;

import com.beginner.domain.Recipe;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("recipesDao") public class MongoRecipesDao {
  @Value("#{mongoContentDBTemplate}") private MongoTemplate mongoTemplate;

  private static final String RECIPE_COLLECTION_NAME = "Recipes";

  public void insertRecipe(Recipe recipe) {
    mongoTemplate.insert(recipe, RECIPE_COLLECTION_NAME);
  }

  public List<Recipe> getRecipes() {
    return mongoTemplate.findAll(Recipe.class, RECIPE_COLLECTION_NAME);
  }

  public Recipe getRecipe(String id) {
    return mongoTemplate.findById(id, Recipe.class, RECIPE_COLLECTION_NAME);
  }

  public void updateRecipe(Recipe recipe) {
    Update update = new Update();
    update.set("name", recipe.getName());
    update.set("ingredients", recipe.getIngredients());
    update.set("description", recipe.getDescription());
    update.set("author", recipe.getAuthor());
    Query query = new Query();
    query.addCriteria(Criteria.where("_id").is(recipe.getId()));
    mongoTemplate.updateFirst(query, update, RECIPE_COLLECTION_NAME);
  }

  public void removeRecipe(String id) {
    Query query = Query.query(Criteria.where("_id").is(id));
    mongoTemplate.remove(query, RECIPE_COLLECTION_NAME);
  }
}

