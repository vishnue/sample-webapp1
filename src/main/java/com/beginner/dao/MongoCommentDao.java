package com.beginner.dao;

import com.beginner.domain.Comment;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("commentDao") public class MongoCommentDao {
  @Value("#{mongoContentDBTemplate}") private MongoTemplate mongoTemplate;

  private static final String COMMENT_COLLECTION_NAME = "Comment";

  public void insertComment(Comment comment) {
    mongoTemplate.insert(comment, COMMENT_COLLECTION_NAME);
  }

  public List<Comment> getComment() {
    return mongoTemplate.findAll(Comment.class, COMMENT_COLLECTION_NAME);
  }

  public Comment getComment(String id) {
    return mongoTemplate.findById(id, Comment.class, COMMENT_COLLECTION_NAME);
  }



  public void removeComment(String id) {
    Query query = Query.query(Criteria.where("_id").is(id));
    mongoTemplate.remove(query, COMMENT_COLLECTION_NAME);
  }
}
