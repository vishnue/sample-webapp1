package com.beginner.domain;

public class Comment {
  private String id;
  //private String comment;
  private String description;
  private String name;
  private String rid;


  public Comment() {

  }

  public Comment(String id, String description,String name,String rid) {
    this.id = id;
    this.description = description;
    this.name=name;
    this.rid=rid;

  }


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }
  public String getName(){return name;}
  public String getRid(){return rid;}



}
