package com.beginner.domain;

public class Recipe {
  private String id;
  private String ingredients;
  private String description;
  private String name;
  private String author;

  public Recipe() {

  }

  public Recipe(String id, String name, String ingredients,
      String author) {
    this.id = id;
    this.ingredients = ingredients;
    this.name = name;
    this.author=author;
  }
  public String getAuthor(){
    return author;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getIngredients() {
    return ingredients;
  }

  public String getDescription() {
    return description;
  }

  public String getName() {
    return name;
  }
}
